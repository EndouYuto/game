#include "PlayerBullet.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "Player.h"
#include "Enemy.h"

//コンストラクタ
PlayerBullet::PlayerBullet(GameObject* parent)
	:GameObject(parent, "PlayerBullet"), hModel_(-1),
	SPEED(0.25f)
{
}

//デストラクタ
PlayerBullet::~PlayerBullet()
{
}

//初期化
void PlayerBullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);

	transform_.rotate_ = Player::GetPlayerRote();
	transform_.rotate_.vecX += 180;
}

//更新
void PlayerBullet::Update()
{
	//移動
	transform_.position_ += move_;
	//SPEEDで弾の速度変える

	/*
	//プレイヤーから敵にホーミング
	PlayerPos = transform_.position_;		//敵の位置
	EnemyPos = Enemy::GetEnemyPos();		//プレイヤーの位置
	PtoE = EnemyPos - PlayerPos;
	transform_.position_ += PtoE * speed;
	*/

	//まっすぐ飛ばしてある程度まで行ったら消える
	if (transform_.position_.vecX > 25 || transform_.position_.vecX < -25 || transform_.position_.vecZ > 25 || transform_.position_.vecZ < -25)
	{
		KillMe();
	}

	//敵に5回当たったら弾消える
	if (enemyHit == 5)
	{
		KillMe();
	}
}

//描画
void PlayerBullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void PlayerBullet::Release()
{
}

//何かに当たった時
void PlayerBullet::OnCollision(GameObject* pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Enemy")
	{
		KillMe();
	}
}

//発射
void PlayerBullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	//位置
	transform_.position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	move_ = XMVector3Normalize(direction) * SPEED;
}