#include "Enemy.h"
#include "../Engine/Model.h"
#include "../Engine/BoxCollider.h"
#include "../Engine/Input.h"
#include "../Engine/SceneManager.h"
#include "EnemyBullet.h"
#include "Player.h"
#include "Hammer.h"
#include "FaceAssist.h"
#include <iostream>
#include <time.h>
#include <math.h>

XMVECTOR Enemy::EnemyPos;
XMVECTOR Enemy::EnemyRote;
//Transform GameObject::transform_;

//コンストラクタ
Enemy::Enemy(GameObject * parent)
	:GameObject(parent, "Enemy"), hModel_(-1)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("PlayerShot.fbx");
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_ = XMVectorSet(0, 0, 4, 0);
	transform_.rotate_.vecY += 180;

	//箱型の当たり判定を作る
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0.5f, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	//ランダム
	srand(time(NULL));

	Instantiate<FaceAssist>(this);
}

//更新
void Enemy::Update()
{
	EnemyPos = transform_.position_;	//Enemyクラスに渡す用の情報
	EnemyRote = transform_.rotate_;	//EnemyBulletクラスに渡す用の情報

	//プレイヤーが向いている方向に回転させる行列
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);

	//移動ベクトル
	XMVECTOR  moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);
	XMVECTOR  moveX = XMVector3TransformCoord(XMVectorSet(0.1f, 0, 0, 0), rotateMatrix);
	
	//回転して弾当たった時の挙動とか確認
	//transform_.rotate_.vecY += 1;
	//transform_.position_ += moveZ;

	if (Input::IsKey(DIK_F))
	{
		int kakunin = 0;
	}

	Flame();
	MapJudge();	//ここの中にMove()入ってる
	Face();
	Shot();
}

//ランダムとったりする
void Enemy::Flame()
{
	//0.5秒ごと
	if (timef % 60 == 0)
	{
		//0.5秒ごとにランダムに数字出す
		MoveRand = rand() % 8;	//移動
	}

	//1フレームごと
	if (timef % 30 == 0)
	{
		//1フレームごとにランダムに数字出す
		ShotRand = rand() % 4;	//撃つ
	}

	timef += 1;

	if (timef == 60)
	{
		timef = 0;
	}
}

//マップ内か外か
void Enemy::MapJudge()
{

	//端かどうか
	if (transform_.position_.vecZ >= 18 || transform_.position_.vecZ <= -18 || transform_.position_.vecX >= 18 || transform_.position_.vecX <= -18)
	{
		//MoveRand = rand() % 8;
		edge = 1;
	}

	//端じゃないなら
	if (edge == 0)
	{
		Move();
	}

	//端なら
	//elseでもよくね
	if (edge == 1)
	{
		EdgeMove();
	}

	//マップ外に行くと死亡
	if (transform_.position_.vecZ >= 20 || transform_.position_.vecZ <= -20 || transform_.position_.vecX >= 20 || transform_.position_.vecX <= -20)
	{
		//KillMe();
	}
}

//何かに当たった
void Enemy::OnCollision(GameObject * pTarget)
{
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);
	XMVECTOR moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);

	//当たったときの処理
	//向いてる方向と逆に下がる
	if (pTarget->GetObjectName() == "PlayerBullet")
	{
		//吹っ飛ばし率アップ
		//Fly += 0.1f;

		//5回当たったら死ぬ
		HP -= 1;

		if (HP == 0)
		{
			KillMe();

			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVERWIN);
		}

		//吹っ飛ぶ
		transform_.position_ -= moveZ;	// *(5 + Fly);
	}

	if (pTarget->GetObjectName() == "Hammer")
	{
		transform_.position_ -= moveZ;	// *(1 + Fly);
	}
}

//プレイヤーと同じ挙動になるか
void Enemy::Move()
{
	//プレイヤーの距離に合わせて向かってくるなど追加

	//見た目汚いからほかの方法ないか
	if (edge == 0)
	{
		switch (MoveRand)
		{
		case 0:
			transform_.position_.vecZ += 0.1f;
			break;

		case 1:
			transform_.position_.vecZ += 0.1f;
			transform_.position_.vecX += 0.1f;
			break;

		case 2:
			transform_.position_.vecX += 0.1f;
			break;

		case 3:
			transform_.position_.vecZ -= 0.1f;
			transform_.position_.vecX += 0.1f;
			break;

		case 4:
			transform_.position_.vecZ -= 0.1f;
			break;

		case 5:
			transform_.position_.vecZ -= 0.1f;
			transform_.position_.vecX -= 0.1f;
			break;

		case 6:
			transform_.position_.vecX -= 0.1f;
			break;

		case 7:
			transform_.position_.vecZ += 0.1f;
			transform_.position_.vecX -= 0.1f;
			break;
		}
	}

	//プレイヤーのところに来る
	//transform_.position_ += PtoE * speed;
}

//マップの端の時の動き
void Enemy::EdgeMove()
{
	//端にいると変な挙動になるからそれ回避
	//もっと自然になるようにしたい
	if (transform_.position_.vecZ >= 8)
	{
		transform_.position_.vecZ -= 0.1f;
	}

	else if (transform_.position_.vecZ <= -8)
	{
		transform_.position_.vecZ += 0.1f;
	}

	else if (transform_.position_.vecX >= 8)
	{
		transform_.position_.vecX -= 0.1f;
	}

	else if (transform_.position_.vecX <= 8)
	{
		transform_.position_.vecX += 0.1f;
	}

	edge = 0;
}

//プレイヤーの方向を向く 距離が測れる
void Enemy::Face()
{
	//場所(仮)
	OriEnemyPos = transform_.position_;		//敵の位置
	PlayerPos = Player::GetPlayerPos();		//プレイヤーの位置
	AssistPos = FaceAssist::GetAssistPos();	//アシストの位置

	//ベクトル
	//敵・プレイヤー
	EtoP = OriEnemyPos - PlayerPos;
	PtoE = PlayerPos - OriEnemyPos;
	//敵・アシスト
	EtoA = OriEnemyPos - AssistPos;
	AtoE = AssistPos - OriEnemyPos;

	//角度取得
	radian = atan2(PlayerPos.vecX - OriEnemyPos.vecX, PlayerPos.vecZ - OriEnemyPos.vecZ);
	radian /= 3.1415926535;
	radian *= 180;
	radian += 360;
	angle = (short)radian % 360;

	transform_.rotate_.vecY = angle;

	//確認用
	if (Input::IsKey(DIK_G))
	{
		int i = 0;
	}

	//距離とってあるけど片方ずつだから計算して
	distanceX = XMVector3Length(EtoP).vecX;	//敵からプレイヤーまでの距離
	distanceZ = XMVector3Length(EtoP).vecZ;	//敵からプレイヤーまでの距離

	//GameObjectでtransformをパブリックに
	//したから直してね
}

//遠距離攻撃
void Enemy::Shot()
{
	//3発までチャージ可能
	//弾の速度によって難易度がガラッと変わるから注意して
	//発射
	if (NoB >= 1)
	{
		//1/3の確率で撃つ(今は一気にたまってる分撃っちゃう)
		//randのとこでshot=0にしてここでは+1するだけ
		if (ShotRand % 3 == 1)
		{
			//弾をシーンに追加
			GameObject* playScene = FindObject("PlayScene");
			EnemyBullet* pEnemyBullet = Instantiate<EnemyBullet>(FindObject("PlayScene"));

			//キャラの先端と根本の位置を取得
			XMVECTOR shotPos = Model::GetBonePosition(hModel_, "Front");		//先端
			XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "Center");	//根本

			//撃つ
			pEnemyBullet->Shot(shotPos, shotPos - cannonRoot);
			shot = 1;
		}

		//弾撃ったら弾数減らす
		if (shot == 1)
		{
			NoB -= 1;
		}

		shot = 0;
	}

	//弾数が3以下なら2秒ごとに補充
	if (timef % 120 == 0)
	{
		if (NoB < 3)
		{
			NoB += 1;
		}
	}
}

//描画
void Enemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Enemy::Release()
{
}