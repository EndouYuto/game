#pragma once
#include "../Engine/GameObject.h"


//Playシーンを管理するクラス
class PlayScene : public GameObject
{
	int time = 0;				//指定した秒数ごとに切り替える
	int time2 = 0;				

	int NoB = 3;
	int NoD = 1;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Shot();
};