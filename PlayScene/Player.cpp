#include "Player.h"
#include "Map.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/BoxCollider.h"
#include "../Engine/SceneManager.h"
#include "PlayerBullet.h"
#include "Hammer.h"
#include "Controller.h"
#include <math.h>

XMVECTOR Player::PlayerPos;
XMVECTOR Player::PlayerRote;
//XMVECTOR Player::GetPlayerPos();

//コンストラクタ
Player::Player(GameObject* parent)
	:GameObject(parent, "Player"), hModel_(-1)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("PlayerShot.fbx");	//手つき・弾出る方向指定ver
	assert(hModel_ >= 0);

	//初期位置
	transform_.position_ = XMVectorSet(0, 0, -4, 0);

	//箱型の当たり判定を作る
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0.5f, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);
}

//更新
void Player::Update()
{
	MapJudge();
	Move();
	Jump();
	Shot();
	Melee();

	PlayerPos = transform_.position_;	//Enemyクラスに渡す用の情報
	PlayerRote = transform_.rotate_;	//Hammerクラスに渡す用の情報

	time += 1;

	if (Dati == 1)
	{
		//ダッシュしたらクールタイム
		Dact += 1;

		if (Dact == 60)
		{
			//1秒たったらダッシュできるように
			Dati = 0;
			Dact = 1;
		}
	}

	if (time == 121)
	{
		time = 0;
	}
}

//マップ内か外か
void Player::MapJudge()
{
	//マップ外に行くと落ちて死亡
	if (transform_.position_.vecZ >= 20 || transform_.position_.vecZ <= -20 || transform_.position_.vecX >= 20 || transform_.position_.vecX <= -20)
	{
		//KillMe();
		fall = 1;
	}

	//Yが0よりも下に行けるように だんだん落ちる
	if (fall == 1)
	{
		Fall();
		ground = 1;
	}
}

//動き
void Player::Move()
{
	//プレイヤーが向いている方向に回転させる行列
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);

	//移動ベクトル
	XMVECTOR  moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);
	XMVECTOR  moveX = XMVector3TransformCoord(XMVectorSet(0.1f, 0, 0, 0), rotateMatrix);

	//普通の速度にスピード掛ける
	moveZ = moveZ * DushSpeed;
	moveX = moveX * DushSpeed;

	//---移動---
	//前進
	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += moveZ;
	}

	//後退
	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= moveZ;
	}

	//右移動
	if (Input::IsKey(DIK_D))
	{
		transform_.position_ += moveX;
	}

	//左移動
	if (Input::IsKey(DIK_A))
	{
		transform_.position_ -= moveX;
	}

	//---ダッシュ---
	//ダッシュしてない&できる状態ならダッシュ
	if (Dati == 0)
	{
		if (Input::IsKeyDown(DIK_LSHIFT) && Dush == 0)
		{
			DushSpeed = 3;	//スピード
			Dush = 1;		//ダッシュしてるか
		}
	}

	//ダッシュボタン押されたら
	if (Dush == 1)
	{
		DushSpeed -= 0.05f;

		if (DushSpeed <= 1)
		{
			DushSpeed = 1;
			Dush = 0;
			Dati = 1;
		}
	}

	//---回転---
	//右回転
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += 1.5f;
	}

	//左回転
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= 1.5f;
	}
}

//float distance = sqrt((point1.x - point2.x) * (point1.x - point2.x) + (point1.y - point2.y) * (point1.y - point2.y) +(point1.z - point2.z) * (point1.z - point2.z));

//当たった時
void Player::OnCollision(GameObject* pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "MoveWall")
	{
		HaveShoes = 1;
	}

	//当たったときの処理
	//弾当たったら死ぬ
	//後ろに下がるに変更するかも
	if (pTarget->GetObjectName() == "EnemyBullet")
	{
		//5回当たると死ぬ
		HP -= 1;

		if (HP == 0)
		{
			KillMe();

			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
		}

		//KillMe();

		//吹っ飛ぶ
		//transform_.position_ -= moveZ;	// * (5 + Fly);
	}
}

//ジャンプ
void Player::Jump()
{
	//今のところ壁に当たると靴手に入れてジャンプできるようになる
	//ジャンプ回数3回まで

	//地面の上なら
	if (ground == 0)
	{
		//ジャンプできるなら
		if (JumpTime < 3)
		{
			//SPACE押すとジャンプ
			if (Input::IsKeyDown(DIK_SPACE))
			{
				JumpPower = 0.7f;
				JumpTime += 1;
			}
		}

		//ジャンプの処理
		transform_.position_.vecY += JumpPower;
		JumpPower -= 0.07f;

		//ジャンプパワーの最低値 落下の最高速度
		if (JumpPower < -0.25f)
		{
			JumpPower = -0.25f;
		}

		//地面に着地したら0に戻る
		//ジャンプ回数も復活
		if (transform_.position_.vecY < 0)
		{
			transform_.position_.vecY = 0;
			JumpTime = 0;
		}
	}
}

//遠距離攻撃
void Player::Shot()
{
	//3発までチャージ可能
	//弾の速度によって難易度がガラッと変わるから注意して
	//発射
	if (NoB >= 1)
	{
		if (Input::IsKeyDown(DIK_E))
		{
			//弾をシーンに追加
			GameObject* playScene = FindObject("PlayScene");
			PlayerBullet* pPlayerBullet = Instantiate<PlayerBullet>(FindObject("PlayScene"));

			//キャラの先端と根本の位置を取得
			XMVECTOR shotPos = Model::GetBonePosition(hModel_, "Front");		//先端
			XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "Center");	//根本

			//撃つ
			pPlayerBullet->Shot(shotPos, shotPos - cannonRoot);
			NoB -= 1;
			time = 1;
		}
	}

	//弾数が3以下なら2秒ごとに補充
	if (time % 120 == 0)
	{
		if (NoB < 3)
		{
			NoB += 1;
		}
	}
	//時間延ばしてもいいかも
	//これから次第
}

//近接攻撃
void Player::Melee()
{
	//前に突っ込みながら攻撃するようにしたい

	//発射
	if (Input::IsKeyDown(DIK_Q))
	{
		//弾をシーンに追加
		GameObject* playScene = FindObject("PlayScene");
		Hammer* pHammer = Instantiate<Hammer>(FindObject("PlayScene"));

		//キャラの先端と根本の位置を取得
		XMVECTOR shotPos = Model::GetBonePosition(hModel_, "Front");		//先端
		XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "Center");	//根本

		//撃つ
		pHammer->Shot(shotPos, shotPos - cannonRoot);
	}
}

//落ちる
void Player::Fall()
{
	transform_.position_.vecY -= 0.1f;

	//落ちると死んでゲームオーバーシーンに移行
	if (transform_.position_.vecY <= -5)
	{
		KillMe();

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}
}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}