#include "DashUi.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "Player.h"

//コンストラクタ
DashUi::DashUi(GameObject* parent)
	:GameObject(parent, "DashUi"), hPict_(-1)
{
}

//デストラクタ
DashUi::~DashUi()
{
}

//初期化
void DashUi::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("dash.png");
	assert(hPict_ >= 0);

	transform_.position_ = XMVectorSet(-0.9f, -0.8f, 0, 0);

	transform_.scale_.vecX = 0.5f;
	transform_.scale_.vecY = 0.5f;
	transform_.scale_.vecZ = 0.5f;
}

//更新
void DashUi::Update()
{
	Shot();

	time += 1;

	if (time == 121)
	{
		time = 0;
	}
}

//描画
void DashUi::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//いつ消えるか判定のために
void DashUi::Shot()
{
	if (NoB >= 1)
	{
		if (Input::IsKeyDown(DIK_LSHIFT))
		{
			NoB -= 1;
		}
	}

	if (time % 120 == 0)
	{
		if (NoB < 1)
		{
			NoB += 1;
		}
	}

	//弾が0個になったとき消える
	if (NoB == 0)
	{
		if (Input::IsKeyDown(DIK_LSHIFT))
		{
			KillMe();
		}
	}
}

//開放
void DashUi::Release()
{
}