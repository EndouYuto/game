#include "BulletUi3.h"
#include "../Engine/Image.h"
#include "../Engine/Input.h"
#include "Player.h"

//コンストラクタ
BulletUi3::BulletUi3(GameObject * parent)
	:GameObject(parent, "BulletUi3"), hPict_(-1)
{
}

//デストラクタ
BulletUi3::~BulletUi3()
{
}

//初期化
void BulletUi3::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("BulletUi.png");
	assert(hPict_ >= 0);

	transform_.position_ = XMVectorSet(0.84f, -0.5f, 0, 0);

	transform_.scale_.vecX = 0.2f;
	transform_.scale_.vecY = 0.3f;
}

//更新
void BulletUi3::Update()
{
	Shot();

	time += 1;

	if (time == 121)
	{
		time = 0;
	}
}

//描画
void BulletUi3::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//いつ消えるか判定のために
void BulletUi3::Shot()
{
	if (NoB >= 1)
	{
		if (Input::IsKeyDown(DIK_E))
		{
			NoB -= 1;
		}
	}

	if (time % 120 == 0)
	{
		if (NoB < 3)
		{
			NoB += 1;
		}
	}

	//弾が2個になったとき消える
	if (NoB == 2)
	{
		if (Input::IsKeyDown(DIK_E))
		{
			KillMe();
		}
	}
}

//開放
void BulletUi3::Release()
{
}