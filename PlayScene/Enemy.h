#pragma once
#include "../Engine/GameObject.h"
#include "Player.h"

class Player;

//◆◆◆を管理するクラス
class Enemy : public GameObject
{
	int hModel_;    //モデル番号

	int HP = 10;			//体力

	int timef = 0;		//確認用 1フレームごとに追加するやつ
	//int ka2 = 0;		//確認用 1フレームごとに追加するやつ いらないかも
	int MoveRand;		//移動につかうランダム
	int ShotRand;		//ショットにつかうランダム
	int shot = 0;		//弾撃ってるか
	int edge = 0;		//端にいるかどうか

	float distanceX;	//プレイヤーと敵の距離
	float distanceZ;	//プレイヤーと敵の距離
	float dx;			//Xの距離
	float dz;			//Yの距離

	float dot;			//内積
	short angle;

	double radian;
	double degree;

	XMMATRIX m;

	float speed = 0.1f;

	float Fly;			//ふっとび力
	float spd = 0.2f;	//確認用
	int	  NoB = 3;		//弾数 (Number of bullets)

	//プレイヤー・敵の位置
	XMVECTOR OriEnemyPos;
	XMVECTOR PlayerPos;
	XMVECTOR AssistPos;

	XMVECTOR EnemyDiv;
	XMVECTOR PlayerDiv;

	//いらないのは今後消して
	XMVECTOR EtoP;	//敵からプレイヤー
	XMVECTOR PtoE;	//プレイヤーから敵
	XMVECTOR EtoA;	//敵からアシスト
	XMVECTOR AtoE;	//アシストから敵

public:
	//コンストラクタ
	Enemy(GameObject* parent);

	//デストラクタ
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//敵の位置
	static XMVECTOR EnemyPos;
	static XMVECTOR GetEnemyPos() { return EnemyPos; }

	//敵の向き
	static XMVECTOR EnemyRote;
	static XMVECTOR GetEnemyRote() { return EnemyRote; }

	void Flame();
	void MapJudge();
	void Move();
	void EdgeMove();
	void Shot();
	void Face();
	void OnCollision(GameObject* pTarget);
};