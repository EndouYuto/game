#include "Treasure.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"

//コンストラクタ
Treasure::Treasure(GameObject * parent)
	:GameObject(parent, "Treasure"), hModel_(-1)
{
}

//デストラクタ
Treasure::~Treasure()
{
}

//初期化
void Treasure::Initialize()
{
	//モデルデータのロード
	//見た目おかしいから間違ってるかも
	//Mayaで直す
	hModel_ = Model::Load("Treasure.fbx");
	assert(hModel_ >= 0);
}

//更新
void Treasure::Update()
{
}

//描画
void Treasure::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Treasure::Release()
{
}