#pragma once
#include "../Engine/GameObject.h"

//◆◆◆を管理するクラス
class DashUi : public GameObject
{
	int hPict_;    //画像番号
	int time = 0;
	int NoB = 1;

public:
	//コンストラクタ
	DashUi(GameObject* parent);

	//デストラクタ
	~DashUi();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void Shot();
};