#include "EnemyBullet.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "Player.h"
#include "Enemy.h"
#include "Hammer.h"

//コンストラクタ
EnemyBullet::EnemyBullet(GameObject* parent)
	:GameObject(parent, "EnemyBullet"), hModel_(-1),
	SPEED(0.2f)
{
}

//デストラクタ
EnemyBullet::~EnemyBullet()
{
}

//初期化
void EnemyBullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);

	transform_.rotate_ = Enemy::GetEnemyRote();
	transform_.rotate_.vecX += 180;
}

//更新
void EnemyBullet::Update()
{
	//移動
	if (BulletMove == 1)
	{
		transform_.position_ += PtoE * SPEED;
	}
	//SPEEDで弾の速度変える

	//敵からプレイヤーにホーミング)
	else if(BulletMove != 1)
	{
		EnemyPos = transform_.position_;		//敵の位置
		PlayerPos = Player::GetPlayerPos();		//プレイヤーの位置
		PtoE = PlayerPos - EnemyPos;
		PtoE = XMVector3Normalize(PtoE);
		transform_.position_ += PtoE * SPEED;
	}

	if (BulletTime > 0)
	{
		BulletTime -= 0.1f;
	}

	else if(BulletTime <= 0)
	{
		BulletMove = 1;
	}

	//まっすぐ飛ばしてある程度まで行ったら消える
	if (transform_.position_.vecX > 25 || transform_.position_.vecX < -25 || transform_.position_.vecZ > 25 || transform_.position_.vecZ < -25)
	{
		KillMe();
	}

	//敵に5回当たったら弾消える
	if (enemyHit == 5)
	{
		KillMe();
	}
}

//描画
void EnemyBullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void EnemyBullet::Release()
{
}

//何かに当たった時
void EnemyBullet::OnCollision(GameObject* pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Player")
	{
		KillMe();
	}

	//当たったときの処理
	if (pTarget->GetObjectName() == "Hammer")
	{
		KillMe();
	}
}

//発射
void EnemyBullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	//位置
	transform_.position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	move_ = XMVector3Normalize(direction) * SPEED;
}