#include "Bullet.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "Player.h"
#include "Enemy.h"

//コンストラクタ
Bullet::Bullet(GameObject * parent)
	:GameObject(parent, "Bullet"), hModel_(-1),
	SPEED(0.25f), GRAVITY(0.00001f)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void Bullet::Update()
{
	//移動
	transform_.position_ += move_;
	//move_で弾の速度変える

	/*
	//敵からプレイヤーにホーミング
	EnemyPos = transform_.position_;		//敵の位置
	PlayerPos = Player::GetPlayerPos();		//プレイヤーの位置
	PtoE = PlayerPos - EnemyPos;
	transform_.position_ += PtoE * speed;
	*/

	/*
	//プレイヤーから敵にホーミング
	PlayerPos = transform_.position_;		//敵の位置
	EnemyPos = Enemy::GetEnemyPos();		//プレイヤーの位置
	PtoE = EnemyPos - PlayerPos;
	transform_.position_ += PtoE * speed;
	*/

	//重力
	//move_.vecY += dy_;
	//dy_ -= GRAVITY;

	//まっすぐ飛ばしてある程度まで行ったら消える
	if (transform_.position_.vecX > 15 || transform_.position_.vecX < -15 || transform_.position_.vecZ > 15 || transform_.position_.vecZ < -15)
	{
		KillMe();
	}

	//敵に5回当たったら弾消える
	if (enemyHit == 5)
	{
		KillMe();
	}
}

//描画
void Bullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

//何かに当たった時
void Bullet::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Enemy")
	{
		enemyHit += 1;
	}
}

//発射
void Bullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	//位置
	transform_.position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	move_ = XMVector3Normalize(direction) * SPEED;
}