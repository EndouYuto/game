#pragma once
#include "../Engine/GameObject.h"

//◆◆◆を管理するクラス
class FaceAssist : public GameObject
{
    int hModel_;    //モデル番号

public:
    //コンストラクタ
    FaceAssist(GameObject* parent);

    //デストラクタ
    ~FaceAssist();

    //初期化
    void Initialize() override;

    //更新
    void Update() override;

    //描画
    void Draw() override;

    //開放
    void Release() override;

    //球の位置
    static XMVECTOR AssistPos;
    static XMVECTOR GetAssistPos() { return AssistPos; }
};