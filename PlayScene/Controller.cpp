#include "Controller.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "Player.h"

//コンストラクタ
Controller::Controller(GameObject * parent)
	:GameObject(parent, "Controller")
{
}

//デストラクタ
Controller::~Controller()
{
}

//初期化
void Controller::Initialize()
{
	transform_.position_ = XMVectorSet(0, 3, -4, 0);
	transform_.rotate_.vecX = 20;
}

//更新
void Controller::Update()
{

	Move();
	if (Dati == 1)
	{
		Dact += 1;

		if (Dact == 61)
		{
			Dati = 0;
			Dact = 1;
		}
	}

	/*
	//右回転
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += 1.5f;
	}

	//左回転 
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= 1.5f;
	}

	//上回転 
	if (Input::IsKey(DIK_UP))
	{
		transform_.rotate_.vecX -= 1;
		if (transform_.rotate_.vecX >= 89)
		{
			transform_.rotate_.vecX = 89;
		}
	}

	//下回転
	if (Input::IsKey(DIK_DOWN))
	{
		transform_.rotate_.vecX += 1;
		if (transform_.rotate_.vecX <= 0)
		{
			transform_.rotate_.vecX = 0;
		}
	}

	//キャラについてくダッシュ
	if (Dati == 0)
	{
		if (Input::IsKeyDown(DIK_LSHIFT) && Dush == 0)
		{
			DushSpeed = 3;	//スピード
			Dush = 1;		//ダッシュしてるか
		}
	}

	//移動関係
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));

	XMVECTOR right{ 0.1f, 0, 0, 0 };
	XMVECTOR front{ 0, 0, 0.1f, 0 };

	right = XMVector3TransformCoord(right, matY);
	front = XMVector3TransformCoord(front, matY);

	//ダッシュしたときの速度
	front = front * DushSpeed;
	right = right * DushSpeed;

	//前進
	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += front;
	}

	//後退
	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= front;
	}

	//右移動
	if (Input::IsKey(DIK_D))
	{
		transform_.position_ += right;
	}

	//左移動
	if (Input::IsKey(DIK_A))
	{
		transform_.position_ -= right;
	}

	//ダッシュボタン押したら
	if (Dush == 1)
	{
		DushSpeed -= 0.05f;

		//ダッシュ中
		if (DushSpeed < 1)
		{
			DushSpeed = 1;
			Dush = 0;
			Dati = 1;
		}
	}

	//カメラ関係
	XMVECTOR camArm = XMVectorSet(0, 0, -10, 0); // Player::GetPosition(); //XMVectorSet(0, 0, -10, 0);
	camArm = XMVector3TransformCoord(camArm, matX * matY);

	Camera::SetPosition(transform_.position_ + camArm);
	Camera::SetTarget(transform_.position_);
	*/
}

void Controller::Move()
{
	//プレイヤーが向いている方向に回転させる行列
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);

	//移動ベクトル
	XMVECTOR  moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);
	XMVECTOR  moveX = XMVector3TransformCoord(XMVectorSet(0.1f, 0, 0, 0), rotateMatrix);

	//普通の速度にスピード掛ける
	moveZ = moveZ * DushSpeed;
	moveX = moveX * DushSpeed;

	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));

	//---移動---
	//前進
	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += moveZ;
	}

	//後退
	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= moveZ;
	}

	//右移動
	if (Input::IsKey(DIK_D))
	{
		transform_.position_ += moveX;
	}

	//左移動
	if (Input::IsKey(DIK_A))
	{
		transform_.position_ -= moveX;
	}

	//---ダッシュ---
	//ダッシュしてない&できる状態ならダッシュ
	if (Dati == 0)
	{
		if (Input::IsKeyDown(DIK_LSHIFT) && Dush == 0)
		{
			DushSpeed = 3;	//スピード
			Dush = 1;		//ダッシュしてるか
		}
	}

	//ダッシュボタン押されたら
	if (Dush == 1)
	{
		DushSpeed -= 0.05f;

		if (DushSpeed <= 1)
		{
			DushSpeed = 1;
			Dush = 0;
			Dati = 1;
		}
	}

	//---回転---
	//右回転
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += 1.5f;
	}

	//左回転
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= 1.5f;
	}

	//カメラ関係
	XMVECTOR camArm = XMVectorSet(0, 0, -10, 0); // Player::GetPosition(); //XMVectorSet(0, 0, -10, 0);
	camArm = XMVector3TransformCoord(camArm, matX * matY);

	Camera::SetPosition(transform_.position_ + camArm);
	Camera::SetTarget(transform_.position_);
}

void Controller::OnCollision(GameObject * pTarget)
{
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);
	XMVECTOR moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);

	//当たったときの処理
	if (pTarget->GetObjectName() == "MoveWall")
	{
		//HaveShoes = 1;
	}

	//当たったときの処理
	//弾当たったら死ぬ
	//後ろに下がるに変更するかも
	if (pTarget->GetObjectName() == "Bullet")
	{
		//KillMe();

		//吹っ飛ぶ
		transform_.position_ -= moveZ * (5 + Fly);
	}
}

//描画
void Controller::Draw()
{
}

//開放
void Controller::Release()
{
}