#pragma once
#include "../Engine/GameObject.h"

//Controllerを管理するクラス
class Controller : public GameObject
{
	int Dush = 0;			//ダッシュしているか
	float DushSpeed = 1;	//ダッシュ力

	int Dati = 0;			//ダッシュできるか・してるか
	int Dact = 0;			//ダッシュした時のCT

	float Fly;				//ふっとび力

public:
	//コンストラクタ
	Controller(GameObject* parent);

	//デストラクタ
	~Controller();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(GameObject * pTarget);

	void Move();
};