#include "FaceAssist.h"
#include "../Engine/Model.h"

XMVECTOR FaceAssist::AssistPos;

//コンストラクタ
FaceAssist::FaceAssist(GameObject* parent)
    :GameObject(parent, "FaceAssist"), hModel_(-1)
{
}

//デストラクタ
FaceAssist::~FaceAssist()
{
}

//初期化
void FaceAssist::Initialize()
{
	//モデルデータのロード
	//hModel_ = Model::Load("assist.fbx");
	//assert(hModel_ >= 0);

	//transform_.position_.vecY += 1;
	//transform_.position_.vecZ += 5;
	transform_.position_ = XMVectorSet(0, 0, 5, 0);
}

//更新
void FaceAssist::Update()
{
	AssistPos = transform_.position_;
	//transform_.position_ = XMVectorSet(0, 0, 5, 0);
}

//描画
void FaceAssist::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void FaceAssist::Release()
{
}