#include "MoveWall.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/BoxCollider.h"

//コンストラクタ
MoveWall::MoveWall(GameObject * parent)
	:GameObject(parent, "MoveWall"), hModel_(-1)
{
}

//デストラクタ
MoveWall::~MoveWall()
{
}

//初期化
void MoveWall::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Wall.fbx");
	assert(hModel_ >= 0);

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	transform_.position_.vecZ = -13;
	transform_.position_.vecX = 8;
}

//更新
void MoveWall::Update()
{
}

//何かに当たった
void MoveWall::OnCollision(GameObject * pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Player")
	{
		transform_.position_.vecY += 5;
		hit = 1;
		//KillMe();
	}
}

//描画
void MoveWall::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void MoveWall::Release()
{
}