#include "JumpWall.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "../Engine/BoxCollider.h"

//コンストラクタ
JumpWall::JumpWall(GameObject * parent)
	:GameObject(parent, "JumpWall"), hModel_(-1)
{
}

//デストラクタ
JumpWall::~JumpWall()
{
}

//初期化
void JumpWall::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Wall.fbx");
	assert(hModel_ >= 0);

	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);
}

//更新
void JumpWall::Update()
{
}

//描画
void JumpWall::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void JumpWall::Release()
{
}