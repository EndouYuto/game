#include "Hammer.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "./Player.h"

//コンストラクタ
Hammer::Hammer(GameObject * parent)
	:GameObject(parent, "Hammer"), hModel_(-1),
	SPEED(0.5f), GRAVITY(0.001f)
{
}

//デストラクタ
Hammer::~Hammer()
{
}

//初期化
void Hammer::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Hammer.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);

	transform_.rotate_ = Player::GetPlayerRote();
	transform_.rotate_.vecY -= 150;
}

//更新
void Hammer::Update()
{
	//回転
	transform_.rotate_.vecY += 1;

	//プレイヤーについてくる
	transform_.position_ = Player::GetPlayerPos();

	//威力強めに
	//出る時間は要調整
	if (time % 60 == 0)
	{
		KillMe();
	}

	time += 1;
}

//描画
void Hammer::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Hammer::Release()
{
}


//発射
void Hammer::Shot(XMVECTOR position, XMVECTOR direction)
{
	//位置
	transform_.position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	move_ = XMVector3Normalize(direction) * SPEED;
}