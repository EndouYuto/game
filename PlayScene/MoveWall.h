#pragma once
#include "../Engine/GameObject.h"

//MoveWallを管理するクラス
class MoveWall : public GameObject
{
	int hModel_;    //モデル番号

	int hit;		//当たったか
	float hy = 0;	//どのくらい動くか

public:
	//コンストラクタ
	MoveWall(GameObject* parent);

	//デストラクタ
	~MoveWall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject *pTarget) override;


	int GetModelHandle() { return hModel_; }
};