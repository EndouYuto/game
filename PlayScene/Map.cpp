#include "Map.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"
#include "Building.h"

//コンストラクタ
Map::Map(GameObject * parent)
	:GameObject(parent, "Map"), hModel_(-1)
{
}

//デストラクタ
Map::~Map()
{
}

//初期化
void Map::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("BattleMap3.fbx");
	assert(hModel_ >= 0);

	for (int i = 0; i < 5; i++)
	{
		GameObject* playScene = FindObject("PlayScene");
		//Building* pBuilding = Instantiate<Building>(FindObject("PlayScene"));
	}

	//transform_.position_.vecY -= 0.5f;
}

//更新
void Map::Update()
{
}

//描画
void Map::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Map::Release()
{
}