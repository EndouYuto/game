#include "Building.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"

//コンストラクタ
Building::Building(GameObject* parent)
	:GameObject(parent, "Building"), hModel_(-1)
{
}

//デストラクタ
Building::~Building()
{
}

//初期化
void Building::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Building.fbx");
	assert(hModel_ >= 0);

	//transform_.position_.vecY -= 0.5f;
}

//更新
void Building::Update()
{
}

//描画
void Building::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Building::Release()
{
}