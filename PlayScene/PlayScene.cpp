#include "PlayScene.h"
#include "Map.h"
#include "Player.h"
#include "Enemy.h"
#include "MoveWall.h"
#include "Treasure.h"
#include "Controller.h"
#include "BulletUi.h"
#include "BulletUi2.h"
#include "BulletUi3.h"
#include "DashUi.h"
#include "BackGround.h"
#include "FaceAssist.h"
#include "../Engine/Input.h"

//コンストラクタ
PlayScene::PlayScene(GameObject * parent)
	: GameObject(parent, "PlayScene")
{
}

//初期化
void PlayScene::Initialize()
{
	Instantiate<BackGround>(this);
	Instantiate<Map>(this);
	Instantiate<Player>(this);
	Instantiate<Enemy>(this);
	//Instantiate<MoveWall>(this);
	//Instantiate<Treasure>(this);
	Instantiate<Controller>(this);
	Instantiate<BulletUi>(this);
	Instantiate<BulletUi2>(this);
	Instantiate<BulletUi3>(this);
	Instantiate<DashUi>(this);
	//Instantiate<FaceAssist>(this);
}

//更新
void PlayScene::Update()
{
	Shot();

	time += 1;
	time2 += 1;

	if (time == 121)
	{
		time = 1;
	}

	if (time2 == 121)
	{
		time2 = 1;
	}
}

//描画
void PlayScene::Draw()
{
}

//弾数画像表示関係
//ここわざわざクラス3つ作ってるから1つでできるようにならないか
void PlayScene::Shot()
{
	//弾数が1になったとき
	if (NoB == 0)
	{
		if (time % 120 == 0)
		{
			Instantiate<BulletUi>(this);
		}
	}

	//弾数が2になったとき
	if (NoB == 1)
	{
		if (time % 120 == 0)
		{
			Instantiate<BulletUi2>(this);
		}
	}

	//弾数が3になったとき
	if (NoB == 2)
	{
		if (time % 120 == 0)
		{
			Instantiate<BulletUi3>(this);
		}
	}

	if (NoD == 0)
	{
		if (time % 120 == 0)
		{
			Instantiate<DashUi>(this);
		}
	}


	if (time % 120 == 0)
	{
		if (NoB < 3)
		{
			NoB += 1;
		}
	}

	if (NoB >= 1)
	{
		if (Input::IsKeyDown(DIK_E))
		{
			NoB -= 1;
			time = 1;
		}
	}

	if (time2 % 120 == 0)
	{
		if (NoD < 1)
		{
			NoD += 1;
		}
	}

	if (NoD >= 1)
	{
		if (Input::IsKeyDown(DIK_LSHIFT))
		{
			NoD -= 1;
		}
	}
}

//開放
void PlayScene::Release()
{
}
