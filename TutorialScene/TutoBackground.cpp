#include "TutoBackground.h"
#include "../Engine/Input.h"
#include "../Engine/Image.h"

//コンストラクタ
TutoBackground::TutoBackground(GameObject* parent)
	: GameObject(parent, "TutoBackground"), hPict_(-1)
{
}

//初期化
void TutoBackground::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("Background.png");
	assert(hPict_ >= 0);

	transform_.scale_.vecX = 3;
	transform_.scale_.vecY = 3;
	transform_.scale_.vecZ = 3;
}

//更新
void TutoBackground::Update()
{
}

//描画
void TutoBackground::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TutoBackground::Release()
{
}