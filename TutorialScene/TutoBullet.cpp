#include "TutoBullet.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "TutoPlayer.h"
#include "TutoEnemy.h"

//コンストラクタ
TutoBullet::TutoBullet(GameObject* parent)
	:GameObject(parent, "TutoBullet"), hModel_(-1),
	SPEED(0.25f)
{
}

//デストラクタ
TutoBullet::~TutoBullet()
{
}

//初期化
void TutoBullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void TutoBullet::Update()
{
	//移動
	transform_.position_ += move_;
	//SPEEDで弾の速度変える

	//まっすぐ飛ばしてある程度まで行ったら消える
	if (transform_.position_.vecX > 15 || transform_.position_.vecX < -15 || transform_.position_.vecZ > 15 || transform_.position_.vecZ < -15)
	{
		KillMe();
	}

	//敵に5回当たったら弾消える
	if (enemyHit == 5)
	{
		KillMe();
	}
}

//描画
void TutoBullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TutoBullet::Release()
{
}

//何かに当たった時
void TutoBullet::OnCollision(GameObject* pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "Enemy")
	{
		enemyHit += 1;
	}
}

//発射
void TutoBullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	//位置
	transform_.position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	move_ = XMVector3Normalize(direction) * SPEED;
}