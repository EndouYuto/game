#include "TutoMelee.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "./TutoPlayer.h"

//コンストラクタ
TutoMelee::TutoMelee(GameObject* parent)
	:GameObject(parent, "TutoMelee"), hModel_(-1),
	SPEED(0.5f), GRAVITY(0.001f)
{
}

//デストラクタ
TutoMelee::~TutoMelee()
{
}

//初期化
void TutoMelee::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Hammer.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);

	transform_.rotate_ = TutoPlayer::GetTutoPlayerRote();
	transform_.rotate_.vecY -= 150;
}

//更新
void TutoMelee::Update()
{
	//回転
	transform_.rotate_.vecY += 1;

	//プレイヤーについてくる
	transform_.position_ = TutoPlayer::GetTutoPlayerPos();

	//威力強めに
	//出る時間は要調整
	if (ka % 60 == 0)
	{
		KillMe();
	}

	ka += 1;
}

//描画
void TutoMelee::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TutoMelee::Release()
{
}


//発射
void TutoMelee::Shot(XMVECTOR position, XMVECTOR direction)
{
	//位置
	transform_.position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	move_ = XMVector3Normalize(direction) * SPEED;
}