#include "TutoMap.h"
#include "../Engine/Model.h"
#include "../Engine/Camera.h"
#include "../Engine/Input.h"

//コンストラクタ
TutoMap::TutoMap(GameObject* parent)
	:GameObject(parent, "TutoMap"), hModel_(-1)
{
}

//デストラクタ
TutoMap::~TutoMap()
{
}

//初期化
void TutoMap::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("BattleMap2.fbx");
	assert(hModel_ >= 0);
}

//更新
void TutoMap::Update()
{
}

//描画
void TutoMap::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TutoMap::Release()
{
}