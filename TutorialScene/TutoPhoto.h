#pragma once
#include "../Engine/GameObject.h"
#include "../Engine/SceneManager.h"


//■■シーンを管理するクラス
class TutoPhoto : public GameObject
{
	int hPict_;    //画像番号
	int PhotoTime = 0;

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	TutoPhoto(GameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};