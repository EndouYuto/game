#include "TutoController.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "TutoPlayer.h"

//コンストラクタ
TutoController::TutoController(GameObject* parent)
	:GameObject(parent, "TutoController")
{
}

//デストラクタ
TutoController::~TutoController()
{
}

//初期化
void TutoController::Initialize()
{
	transform_.rotate_.vecX = 20;
}

//更新
void TutoController::Update()
{
	//向きも初期化できるようにする
	for (ControllerTime = 0; ControllerTime < 5; ControllerTime++)
	{
		if (first == 0)
		{
			transform_.position_ = XMVectorSet(0, 3, -3, 0);
		}
		first = 1;

		if (Input::IsKeyDown(DIK_RETURN))
		{
			ControllerTime += 1;
			first = 0;
		}
	}

	Move();
}

void TutoController::Move()
{
	//プレイヤーが向いている方向に回転させる行列
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);

	//移動ベクトル
	XMVECTOR  moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);
	XMVECTOR  moveX = XMVector3TransformCoord(XMVectorSet(0.1f, 0, 0, 0), rotateMatrix);

	//普通の速度にスピード掛ける
	moveZ = moveZ * DushSpeed;
	moveX = moveX * DushSpeed;

	//---移動---
	//前進
	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += moveZ;
	}

	//後退
	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= moveZ;
	}

	//右移動
	if (Input::IsKey(DIK_D))
	{
		transform_.position_ += moveX;
	}

	//左移動
	if (Input::IsKey(DIK_A))
	{
		transform_.position_ -= moveX;
	}

	//---ダッシュ---
	//ダッシュしてない&できる状態ならダッシュ
	if (Dati == 0)
	{
		if (Input::IsKeyDown(DIK_LSHIFT) && Dush == 0)
		{
			DushSpeed = 3;	//スピード
			Dush = 1;		//ダッシュしてるか
		}
	}

	//ダッシュボタン押されたら
	if (Dush == 1)
	{
		DushSpeed -= 0.05f;

		if (DushSpeed <= 1)
		{
			DushSpeed = 1;
			Dush = 0;
			Dati = 1;
		}
	}

	//---回転---
	//右回転
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += 1.5f;
	}

	//左回転
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= 1.5f;
	}
	
	//移動関係
	XMMATRIX matY = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));
	XMMATRIX matX = XMMatrixRotationX(XMConvertToRadians(transform_.rotate_.vecX));


	//カメラ関係
	XMVECTOR camArm = XMVectorSet(0, 0, -10, 0); // Player::GetPosition(); //XMVectorSet(0, 0, -10, 0);
	camArm = XMVector3TransformCoord(camArm, matX * matY);

	Camera::SetPosition(transform_.position_ + camArm);
	Camera::SetTarget(transform_.position_);
}

void TutoController::OnCollision(GameObject* pTarget)
{
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);
	XMVECTOR moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);

	//当たったときの処理
	if (pTarget->GetObjectName() == "MoveWall")
	{
		//HaveShoes = 1;
	}

	//当たったときの処理
	//弾当たったら死ぬ
	//後ろに下がるに変更するかも
	if (pTarget->GetObjectName() == "Bullet")
	{
		//KillMe();

		//吹っ飛ぶ
		transform_.position_ -= moveZ * (5 + Fly);
	}
}

//描画
void TutoController::Draw()
{
}

//開放
void TutoController::Release()
{
}