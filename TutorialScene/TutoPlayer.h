#pragma once
#include "../Engine/GameObject.h"

//TutoPlayerを管理するクラス
class TutoPlayer : public GameObject
{
	int hModel_;    //モデル番号

	int PlayerTime = 0;

	int first = 0;

	int	  Dush = 0;			//ダッシュしているか
	int   DushOk = 0;		//ダッシュできるか
	float DushSpeed = 1;	//ダッシュ力

	int	  NoB = 3;			//弾数 (Number of bullets)

	int   time = 0;			//指定した秒数ごとに切り替える

	int Dati = 0;			//ダッシュできるか
	int Dact = 0;			//ダッシュした時のクールタイム

	int   JumpTime = 0;		//ジャンプ回数
	float JumpPower = 0;	//ジャンプ力

	float Fly = 0;			//ふっとび力

	int fall = 0;			//落ちてるか 落ちてたらYが1(ぐらい)よりも下に行ける

	int ground = 0;			//ジャンプ(地面)の判定について マップから落ちるときに1にする

	int HaveShoes = 0;		//靴持ってるか
	int haveGun = 0;		//銃持ってるか

	XMVECTOR OriTutoPlayerPos;
	XMVECTOR EnemyPos;
	XMVECTOR EtoP;

	float distance;		//プレイヤーと敵の距離
	float dx;			//Xの距離
	float dz;			//Yの距離

public:
	//コンストラクタ
	TutoPlayer(GameObject* parent);

	//デストラクタ
	~TutoPlayer();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;

	//プレイヤーの位置
	static XMVECTOR TutoPlayerPos;
	static XMVECTOR GetTutoPlayerPos() { return TutoPlayerPos; }

	//プレイヤーの向き
	static XMVECTOR TutoPlayerRote;
	static XMVECTOR GetTutoPlayerRote() { return TutoPlayerRote; }

	void MapJudge();
	void Move();
	void Jump();
	void Shot();
	void Melee();
};