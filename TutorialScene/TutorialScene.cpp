#include "TutorialScene.h"
#include "TutoMap.h"
#include "TutoPlayer.h"
#include "TutoEnemy.h"
#include "TutoController.h"
#include "TutoBackGround.h"
#include "TutoPhoto.h"
#include "../Engine/Input.h"

//コンストラクタ
TutorialScene::TutorialScene(GameObject* parent)
	: GameObject(parent, "TutorialScene")
{
}

//初期化
void TutorialScene::Initialize()
{
	Instantiate<TutoBackground>(this);
	Instantiate<TutoMap>(this);
	Instantiate<TutoPlayer>(this);
	Instantiate<TutoEnemy>(this);
	Instantiate<TutoController>(this);
	Instantiate<TutoPhoto>(this);
}

//更新
void TutorialScene::Update()
{
	Shot();

	time += 1;
	time2 += 1;

	if (time == 121)
	{
		time = 1;
	}

	if (time2 == 121)
	{
		time2 = 1;
	}


	if (Input::IsKeyDown(DIK_G))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TEST);
	}
}

//描画
void TutorialScene::Draw()
{
}

//弾数画像表示関係
//ここわざわざクラス3つ作ってるから1つでできるようにならないか
void TutorialScene::Shot()
{
	if (time % 120 == 0)
	{
		if (NoB < 3)
		{
			NoB += 1;
		}
	}

	if (NoB >= 1)
	{
		if (Input::IsKeyDown(DIK_E))
		{
			NoB -= 1;
			time = 1;
		}
	}

	if (time2 % 120 == 0)
	{
		if (NoD < 1)
		{
			NoD += 1;
		}
	}

	if (NoD >= 1)
	{
		if (Input::IsKeyDown(DIK_LSHIFT))
		{
			NoD -= 1;
		}
	}
}

//開放
void TutorialScene::Release()
{
}
