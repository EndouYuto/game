#pragma once
#include "../Engine/GameObject.h"
#include "TutoPlayer.h"

class TutoPlayer;

//◆◆◆を管理するクラス
class TutoEnemy : public GameObject
{
	int hModel_;    //モデル番号

	int EnemyTime = 0;

	float xmove = 0.1;

	int first = 0;

	int timef = 0;			//確認用 1フレームごとに追加するやつ
	int MoveRand;		//移動につかうランダム
	int ShotRand;		//ショットにつかうランダム
	int shot = 0;		//弾撃ってるか

	float speed = 0.1f;

	float Fly;			//ふっとび力
	float spd = 0.2f;	//確認用

public:
	//コンストラクタ
	TutoEnemy(GameObject* parent);

	//デストラクタ
	~TutoEnemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	static XMVECTOR TutoEnemyPos;
	static XMVECTOR GetTutoEnemyPos() { return TutoEnemyPos; }

	void Flame();
	void Shot();
	void OnCollision(GameObject* pTarget);
};