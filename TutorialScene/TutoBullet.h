#pragma once
#include "../Engine/GameObject.h"

//弾を管理するクラス
class TutoBullet : public GameObject
{

	int hModel_;		//モデル番号
	XMVECTOR move_;		//移動ベクトル
	float dy_;			//Ｙ方向の加速度

	int enemyHit = 0;	//敵に当たった回数 5回当たると弾が消える

	XMVECTOR EnemyPos;
	XMVECTOR PlayerPos;
	XMVECTOR PtoE;
	float speed = 0.1f;

	const float SPEED;		//速度

public:
	//コンストラクタ
	TutoBullet(GameObject* parent);

	//デストラクタ
	~TutoBullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//何かに当たった
	//引数：pTarget 当たった相手
	void OnCollision(GameObject* pTarget) override;


	//発射
	//引数；position	発射位置
	//引数：direction	発射方向
	void Shot(XMVECTOR position, XMVECTOR direction);
};