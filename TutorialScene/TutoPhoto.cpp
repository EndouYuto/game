#include "TutoPhoto.h"
#include "../Engine/Input.h"
#include "../Engine/Image.h"

//コンストラクタ
TutoPhoto::TutoPhoto(GameObject* parent)
	: GameObject(parent, "TutoPhoto"), hPict_(-1)
{
}

//初期化
void TutoPhoto::Initialize()
{
	//画像データのロード
	//hPict_ = Image::Load("Background.png");
	//assert(hPict_ >= 0);

	transform_.scale_.vecX = 0.6;
	transform_.scale_.vecY = 0.6;

	transform_.position_.vecX = -0.9;
	transform_.position_.vecY = -0.5;
}

//更新
void TutoPhoto::Update()
{
	//移動・視点移動
	//WASD・←→
	if (PhotoTime == 0)
	{
		//hPict_ = Image::Load("sousahouhou.png");
		hPict_ = Image::Load("move.png");
		assert(hPict_ >= 0);
		Image::SetAlpha(hPict_, 128);
	}

	//ジャンプ・ダッシュ
	//space・shift
	if (PhotoTime == 1)
	{
		hPict_ = Image::Load("look.png");
		assert(hPict_ >= 0);
		Image::SetAlpha(hPict_, 128);
	}

	//攻撃・ホーミング攻撃
	//E・F+説明
	if (PhotoTime == 2)
	{
		//hPict_ = Image::Load("kougeki.png");
		hPict_ = Image::Load("attack.png");
		assert(hPict_ >= 0);
		Image::SetAlpha(hPict_, 128);
	}

	//近接攻撃
	//Q+説明
	if (PhotoTime == 3)
	{
		//hPict_ = Image::Load("kougeki.png");
		hPict_ = Image::Load("attack2.png");
		assert(hPict_ >= 0);
		Image::SetAlpha(hPict_, 128);
	}

	//チュートリアル完了
	//GでPlaySceneへ
	if (PhotoTime == 4)
	{
		hPict_ = Image::Load("finish.png");
		assert(hPict_ >= 0);
		Image::SetAlpha(hPict_, 128);
	}

	if (Input::IsKeyDown(DIK_RETURN))
	{
		PhotoTime += 1;
	}
}

//描画
void TutoPhoto::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void TutoPhoto::Release()
{
}