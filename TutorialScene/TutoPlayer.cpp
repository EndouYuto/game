#include "TutoPlayer.h"
#include "TutoMap.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"
#include "../Engine/Camera.h"
#include "../Engine/BoxCollider.h"
#include "../Engine/SceneManager.h"
#include "TutoBullet.h"
#include "TutoMelee.h"
#include "TutoController.h"
#include <math.h>

XMVECTOR TutoPlayer::TutoPlayerPos;
XMVECTOR TutoPlayer::TutoPlayerRote;
//XMVECTOR TutoPlayer::GetTutoPlayerPos();

//コンストラクタ
TutoPlayer::TutoPlayer(GameObject* parent)
	:GameObject(parent, "TutoPlayer"), hModel_(-1)
{
}

//デストラクタ
TutoPlayer::~TutoPlayer()
{
}

//初期化
void TutoPlayer::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("PlayerShot.fbx");	//手つき・弾出る方向指定ver
	assert(hModel_ >= 0);

	//箱型の当たり判定を作る
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0.5f, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);
}

//更新
void TutoPlayer::Update()
{
	MapJudge();
	Move();
	Jump();
	Shot();
	Melee();

	TutoPlayerPos = transform_.position_;	//Enemyクラスに渡す用の情報
	TutoPlayerRote = transform_.rotate_;	//Hammerクラスに渡す用の情報

	time += 1;

	if (Dati == 1)
	{
		//ダッシュしたらクールタイム
		Dact += 1;

		if (Dact == 61)
		{
			//1秒たったらダッシュできるように
			Dati = 0;
			Dact = 1;
		}
	}

	if (time == 121)
	{
		time = 1;
	}

	//向きも初期化できるようにする
	for (PlayerTime = 0; PlayerTime < 5; PlayerTime++)
	{
		if (first == 0)
		{
			transform_.position_ = XMVectorSet(0, 0, -3, 0);
		}
		first = 1;

		if (Input::IsKeyDown(DIK_RETURN))
		{
			PlayerTime += 1;
			first = 0;
		}
	}
}

//マップ内か外か
void TutoPlayer::MapJudge()
{
}

//動き
void TutoPlayer::Move()
{
	//プレイヤーが向いている方向に回転させる行列
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);

	//移動ベクトル
	XMVECTOR  moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);
	XMVECTOR  moveX = XMVector3TransformCoord(XMVectorSet(0.1f, 0, 0, 0), rotateMatrix);

	//普通の速度にスピード掛ける
	moveZ = moveZ * DushSpeed;
	moveX = moveX * DushSpeed;

	//---移動---
	//前進
	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += moveZ;
	}

	//後退
	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= moveZ;
	}

	//右移動
	if (Input::IsKey(DIK_D))
	{
		transform_.position_ += moveX;
	}

	//左移動
	if (Input::IsKey(DIK_A))
	{
		transform_.position_ -= moveX;
	}

	//---ダッシュ---
	//ダッシュしてない&できる状態ならダッシュ
	if (Dati == 0)
	{
		if (Input::IsKeyDown(DIK_LSHIFT) && Dush == 0)
		{
			DushSpeed = 3;	//スピード
			Dush = 1;		//ダッシュしてるか
		}
	}

	//ダッシュボタン押されたら
	if (Dush == 1)
	{
		DushSpeed -= 0.05f;

		if (DushSpeed <= 1)
		{
			DushSpeed = 1;
			Dush = 0;
			Dati = 1;
		}
	}

	//---回転---
	//右回転
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.rotate_.vecY += 1.5f;
	}

	//左回転
	if (Input::IsKey(DIK_LEFT))
	{
		transform_.rotate_.vecY -= 1.5f;
	}
}

//当たった時
void TutoPlayer::OnCollision(GameObject* pTarget)
{
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);
	XMVECTOR moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);

	//当たったときの処理
	if (pTarget->GetObjectName() == "MoveWall")
	{
		HaveShoes = 1;
	}

	//当たったときの処理
	//弾当たったら死ぬ
	//後ろに下がるに変更するかも
	if (pTarget->GetObjectName() == "EnemyBullet")
	{
		//吹っ飛ぶ
		//transform_.position_ -= moveZ;
	}
}

//ジャンプ
void TutoPlayer::Jump()
{
	//今のところ壁に当たると靴手に入れてジャンプできるようになる
	//ジャンプ回数3回まで

	//マップ内なら
	if (ground == 0)
	{
		//ジャンプできるなら
		if (JumpTime < 3)
		{
			//SPACE押すとジャンプ
			if (Input::IsKeyDown(DIK_SPACE))
			{
				JumpPower = 0.7f;
				JumpTime += 1;
			}
		}

		//ジャンプの処理
		transform_.position_.vecY += JumpPower;
		JumpPower -= 0.07f;

		//ジャンプパワーの最低値 落下の最高速度
		if (JumpPower < -0.25f)
		{
			JumpPower = -0.25f;
		}

		//地面のに着地したら0に戻る
		//ジャンプ回数も復活
		if (transform_.position_.vecY < 0)
		{
			transform_.position_.vecY = 0;
			JumpTime = 0;
		}
	}
}

//遠距離攻撃
void TutoPlayer::Shot()
{
	if (Input::IsKeyDown(DIK_E))
	{
		//弾をシーンに追加
		GameObject* playScene = FindObject("PlayScene");
		TutoBullet* pTutoBullet = Instantiate<TutoBullet>(FindObject("TutorialScene"));

		//キャラの先端と根本の位置を取得
		XMVECTOR shotPos = Model::GetBonePosition(hModel_, "Front");		//先端
		XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "Center");	//根本

		//撃つ
		pTutoBullet->Shot(shotPos, shotPos - cannonRoot);
	}
}

//近接攻撃
void TutoPlayer::Melee()
{
	//発射
	if (Input::IsKeyDown(DIK_Q))
	{
		//弾をシーンに追加
		GameObject* playScene = FindObject("TutorialScene");
		TutoMelee* pTutoMelee= Instantiate<TutoMelee>(FindObject("TutorialScene"));

		//キャラの先端と根本の位置を取得
		XMVECTOR shotPos = Model::GetBonePosition(hModel_, "Front");		//先端
		XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "Center");	//根本

		//撃つ
		pTutoMelee->Shot(shotPos, shotPos - cannonRoot);
	}
}

//描画
void TutoPlayer::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TutoPlayer::Release()
{
}