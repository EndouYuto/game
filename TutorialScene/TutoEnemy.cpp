#include "TutoEnemy.h"
#include "../Engine/Model.h"
#include "../Engine/BoxCollider.h"
#include "../Engine/Input.h"
#include "TutoPlayer.h"
#include "TutoMelee.h"
#include "TutoEnemyBullet.h"
#include <iostream>
#include <time.h>
#include <math.h>

XMVECTOR TutoEnemy::TutoEnemyPos;
//Transform GameObject::transform_;

//コンストラクタ
TutoEnemy::TutoEnemy(GameObject* parent)
	:GameObject(parent, "TutoEnemy"), hModel_(-1)
{
}

//デストラクタ
TutoEnemy::~TutoEnemy()
{
}

//初期化
void TutoEnemy::Initialize()
{
	//箱型の当たり判定を作る
	BoxCollider* collision = new BoxCollider(XMVectorSet(0, 0.5f, 0, 0), XMVectorSet(1, 1, 1, 0));
	AddCollider(collision);

	transform_.position_ = XMVectorSet(0, 0, 1, 0);
	transform_.rotate_.vecY += 180;

	//ランダム
	srand(time(NULL));
}

//更新
void TutoEnemy::Update()
{
	TutoEnemyPos = transform_.position_;	//TutoEnemyクラスに渡す用の情報

	Flame();

	//ホーミングどういうのか確かめる用
	if (EnemyTime == 2)
	{
		//モデルデータのロード
		hModel_ = Model::Load("PlayerShot.fbx");
		assert(hModel_ >= 0);

		//マップが出でたら初期位置
		if (transform_.position_.vecZ >= 10)
		{
			transform_.position_ = XMVectorSet(0, 0, 1, 0);
		}

		//左右に移動してる
		transform_.position_.vecX += xmove;

		if (timef == 59)
		{
			xmove *= -1;
		}
	}

	//近距離攻撃で弾消せるから一定間隔で弾撃つ
	if (EnemyTime == 3)
	{
		if (timef == 59)
		{
			//弾をシーンに追加
			GameObject* tutorialScene = FindObject("TutorialScene");
			TutoEnemyBullet* pTutoEnemyBullet = Instantiate<TutoEnemyBullet>(FindObject("TutorialScene"));

			//キャラの先端と根本の位置を取得
			XMVECTOR shotPos = Model::GetBonePosition(hModel_, "Front");		//先端
			XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "Center");	//根本

			//撃つ
			pTutoEnemyBullet->Shot(shotPos, shotPos - cannonRoot);
		}
	}

	//次へ
	if (Input::IsKeyDown(DIK_RETURN))
	{
		transform_.position_ = XMVectorSet(0, 0, 1, 0);
		EnemyTime += 1;
		first = 0;
	}

	//初期位置へ
	if (Input::IsKeyDown(DIK_R))
	{
		transform_.position_ = XMVectorSet(0, 0, 1, 0);
	}
}

//ランダムとったりする
void TutoEnemy::Flame()
{
	//1秒ごと
	if (timef % 60 == 0)
	{
		//0.5秒ごとにランダムに数字出す
		MoveRand = rand() % 8;	//移動
	}

	//0.5秒ごと
	if (timef % 30 == 0)
	{
		//1フレームごとにランダムに数字出す
		ShotRand = rand() % 4;	//撃つ
	}

	timef += 1;

	//重くなりそうだから初期化
	if (timef == 60)
	{
		timef = 0;
	}
}

//何かに当たった
void TutoEnemy::OnCollision(GameObject* pTarget)
{
	XMMATRIX rotateMatrix = XMMatrixRotationY(transform_.rotate_.vecY / 180 * 3.14f);
	XMVECTOR moveZ = XMVector3TransformCoord(XMVectorSet(0, 0, 0.1f, 0), rotateMatrix);

	//当たったときの処理
	//向いてる方向の逆に下がる
	if (pTarget->GetObjectName() == "TutoBullet")
	{
		//吹っ飛ぶ
		transform_.position_ -= moveZ;
	}

	//近接攻撃当たった時
	if (pTarget->GetObjectName() == "TutoMelee")
	{
		transform_.position_ -= moveZ;
	}
}

//遠距離攻撃
void TutoEnemy::Shot()
{
	//弾をシーンに追加
	GameObject* tutorialScene = FindObject("TutorialScene");
	TutoEnemyBullet* pTutoEnemyBullet = Instantiate<TutoEnemyBullet>(FindObject("TutorialScene"));

	//キャラの先端と根本の位置を取得
	XMVECTOR shotPos = Model::GetBonePosition(hModel_, "Front");		//先端
	XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "Center");	//根本

	//撃つ
	pTutoEnemyBullet->Shot(shotPos, shotPos - cannonRoot);
}

//描画
void TutoEnemy::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TutoEnemy::Release()
{
}