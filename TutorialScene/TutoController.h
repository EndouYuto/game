#pragma once
#include "../Engine/GameObject.h"

//TutoControllerを管理するクラス
class TutoController : public GameObject
{
	int ControllerTime = 0;
	int first = 0;

	int Dush = 0;			//ダッシュしているか
	float DushSpeed = 1;	//ダッシュ力

	int Dati = 0;			//ダッシュできるか・してるか
	int Dact = 0;			//ダッシュした時のCT

	float Fly;				//ふっとび力

public:
	//コンストラクタ
	TutoController(GameObject* parent);

	//デストラクタ
	~TutoController();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	void OnCollision(GameObject* pTarget);

	void Move();
};