#include "TutoEnemyBullet.h"
#include "../Engine/Model.h"
#include "../Engine/Input.h"

//コンストラクタ
TutoEnemyBullet::TutoEnemyBullet(GameObject* parent)
	:GameObject(parent, "TutoEnemyBullet"), hModel_(-1),
	SPEED(0.25f)
{
}

//デストラクタ
TutoEnemyBullet::~TutoEnemyBullet()
{
}

//初期化
void TutoEnemyBullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0), 0.5f);
	AddCollider(collision);

	transform_.rotate_.vecX += 180;
}

//更新
void TutoEnemyBullet::Update()
{
	transform_.position_ += move_;

	//まっすぐ飛ばしてある程度まで行ったら消える
	if (transform_.position_.vecX > 15 || transform_.position_.vecX < -15 || transform_.position_.vecZ > 15 || transform_.position_.vecZ < -15)
	{
		KillMe();
	}

	//敵に5回当たったら弾消える
	if (enemyHit == 5)
	{
		KillMe();
	}
}

//描画
void TutoEnemyBullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TutoEnemyBullet::Release()
{
}

//何かに当たった時
void TutoEnemyBullet::OnCollision(GameObject* pTarget)
{
	//当たったときの処理
	if (pTarget->GetObjectName() == "TutoPlayer")
	{
		enemyHit += 1;
	}

	//当たったときの処理
	if (pTarget->GetObjectName() == "TutoMelee")
	{
		KillMe();
	}
}

//発射
void TutoEnemyBullet::Shot(XMVECTOR position, XMVECTOR direction)
{
	//位置
	transform_.position_ = position;

	//移動は、引数で渡された方向にSPEEDの速さ
	move_ = XMVector3Normalize(direction) * SPEED;
}