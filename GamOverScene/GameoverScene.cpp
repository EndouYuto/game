#include "GameOverScene.h"
#include "../Engine/Input.h"
#include "../Engine/Image.h"

//コンストラクタ
GameOverScene::GameOverScene(GameObject * parent)
	: GameObject(parent, "GameOverScene"), hPict_(-1)
{
}

//初期化
void GameOverScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("GameOver2.jpg");
	assert(hPict_ >= 0);

	transform_.scale_.vecX = 1.5f;
	transform_.scale_.vecY = 1.5f;
}

//更新
void GameOverScene::Update()
{
	if (Input::IsKey(DIK_SPACE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void GameOverScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameOverScene::Release()
{
}