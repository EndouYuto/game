#include "GameoverWinScene.h"
#include "../Engine/Input.h"
#include "../Engine/Image.h"

//コンストラクタ
GameoverWinScene::GameoverWinScene(GameObject* parent)
	: GameObject(parent, "GameoverWinScene"), hPict_(-1)
{
}

//初期化
void GameoverWinScene::Initialize()
{
	//画像データのロード
	hPict_ = Image::Load("GameOver.jpg");
	assert(hPict_ >= 0);

	transform_.scale_.vecX = 1.5f;
	transform_.scale_.vecY = 1.5f;
}

//更新
void GameoverWinScene::Update()
{
	if (Input::IsKey(DIK_SPACE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}
}

//描画
void GameoverWinScene::Draw()
{
	Image::SetTransform(hPict_, transform_);
	Image::Draw(hPict_);
}

//開放
void GameoverWinScene::Release()
{
}